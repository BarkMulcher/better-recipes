from django.apps import AppConfig


class BettahRecipesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bettah_recipes'
